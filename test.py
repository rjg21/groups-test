from google.oauth2 import service_account
from googleapiclient import discovery
import logging
import json

LOG = logging.getLogger(__name__)

SCOPES = [
    'https://www.googleapis.com/auth/admin.directory.user.readonly',
    'https://www.googleapis.com/auth/admin.directory.group.readonly',
]

GROUPS_DOMAIN = 'groups.lookup.cam.ac.uk'

# Two groups that are failing:
#    103601@groups.lookup.cam.ac.uk
#    103244@groups.lookup.cam.ac.uk
# And group that succeeds:
#    103526@groups.lookup.cam.ac.uk
CHECK_GROUPS = ['103601', '103244', '103526']

PAGE_SIZE = 200


def authenticate():
    LOG.info('Authenticating')
    creds = service_account.Credentials.from_service_account_file('./credentials.json')
    return creds.with_scopes(SCOPES)


def fetch_groups(directory_service):
    LOG.info(f'Fetching groups')
    fields = ['id', 'email', 'name']
    # Loop while we wait for nextPageToken to be "none"
    page_token = None
    resources = []
    while True:
        list_response = directory_service.groups().list(
            domain=GROUPS_DOMAIN,
            fields='nextPageToken,groups(' + ','.join(fields) + ')',
            pageToken=page_token, maxResults=PAGE_SIZE).execute()
        items = list_response.get('groups', [])
        LOG.info(f'...page with {len(items)} items')
        resources.extend(items)

        # Get the token for the next page
        page_token = list_response.get('nextPageToken')
        if page_token is None:
            break

    return resources


def get_group(directory_service, grp):
    LOG.info(f'Getting group: {grp}')
    try:
        return directory_service.groups().get(
            groupKey=f'{grp}@{GROUPS_DOMAIN}'
        ).execute()
    except Exception as e:
        LOG.error(f'Failed with {e}')
    return {}


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

    creds = authenticate()

    # Discover admin directory service
    directory_service = discovery.build('admin', 'directory_v1', credentials=creds)

    # Fetch all groups in GROUPS_DOMAIN
    groups = fetch_groups(directory_service)

    # List of emails of all groups
    emails = {g['email'] for g in groups}

    # Check for existence CHECK_GROUPS
    found = {g for g in CHECK_GROUPS if f'{g}@{GROUPS_DOMAIN}' in emails}
    not_found = set(CHECK_GROUPS) - found

    LOG.info(f'Found groups: {found}')
    LOG.info(f'Not found groups: {not_found}')

    # Try to get groups
    for g in CHECK_GROUPS:
        print(json.dumps(get_group(directory_service, g)))
