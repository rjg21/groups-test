FROM python:3.7-alpine

WORKDIR /usr/src/app

ADD requirements.txt ./
RUN pip install -r requirements.txt

ADD ./test.py ./test.py

ENTRYPOINT ["python", "./test.py"]
