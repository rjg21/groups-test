## Group Failure Tester

Build docker image:
```console
docker build -t groups-test .
```

Credentials with permissions to read groups in domain in `./credentials.json`

Run test
```console
docker run --rm -ti -v ${PWD}/credentials.json:/usr/src/app/credentials.json:ro groups-test
```
